# Card Odds Query Interface
A Next/React app for selecting decks, forming queries, and retrieving the answers from
those queries from the API. 

# Learning Purpose
* Learn React
* Learn Typescript React Engine
* Conduct test-driven development
* Use MUI


## Getting Started


```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
